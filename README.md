The cold air bypass piping connects the cold air bypass valve to the post combustor plenum. It consists of eight straight pipe segments connected by nine pipe fittings of various types.

This sub-assembly is self-contained and does not require specifying any physical parameters.